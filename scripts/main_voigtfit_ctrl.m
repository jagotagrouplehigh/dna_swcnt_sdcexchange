% Fit the absorbtion spectra of a mixture of SWCNTs as a sum of spectra of
% purified SWCNT species using voigt profile.

% 'voigt line shape fit' package and 'fadf' function are required to run 
% this mfile.
% 'voigt line shape fit' package can be downloaded from the following link:
% https://www.mathworks.com/matlabcentral/fileexchange/57603-voigt-line-shape-fit
% 'fadf' function can be downloaded from the following link:
% https://www.mathworks.com/matlabcentral/fileexchange/47801-the-voigt-complex-error-function-second-version

clear all; close all; clc;

% Go to the directory and list all filenames in the directory
cd ../data/SDC_EG150X_40MeOH/control_sample/blcorrected/
filenames = dir;

cd ../../../../scripts/

% wavelength measurement step (usually 0.5 or 1 nm)
wlspan = 1; 
% Set fitting range
fitstart = (1090-1080)/wlspan;     % End wavelength is 1080 nm
fitend = (1090-910)/wlspan;       % Start wavelength is 910 nm


for i = 4:(size(filenames,1)-1)
    % Read csv files. col 1 is wavelength and col 2 is the absorbance. 
    fid = fopen([filenames(i).folder '/' filenames(i).name], 'r');
    rawdata = textscan(fid, '%f%f', 'Delimiter', ',', 'HeaderLines', 1);
    fclose(fid)
    
    % This is the range of wavelengths and absorbance we want to fit
    data(:,1) = rawdata{1}(fitstart:fitend);
    data(:,2) = rawdata{2}(fitstart:fitend);
    
    % Load the initial parameters, par0. 1st row is peak position, 2nd row
    % is intensity height, third row is Gaussian width, and 4th row is
    % Lorentzian width. 
    if isempty(strfind(filenames(i).name,'5C'))
        par0 = load('par0_DNA_EG150X.txt');
    else
        par0 = load('par0_SDC_EG150X.txt');
    end
    
    % Fit the spectra using Voigt profile. 
    [parmin,resnom,res,exitflag]= fit2voigt_restricted(data,par0);
    % Function 'fit2voigt_restricted' is the modifed version of 'fit2voigt'
    % function. We restricted some of the fitting parameters, Lorentzian
    % width and Gaussian width, to be in a range of [-0.5*(original value),
    % 0.5*(original value)]. Thus, the following scripts was changed in
    % 'fit2voigt' function:
    % 
    %%%%% Part of original 'fit2voigt' function:
    % Lb=[0.001 15];  % Lorentzian width range [lower upper]
    % Gb=[0.1 15];     % Gaussian width range [lower upper]
    % maxfeval=150*numel(par0); % maximum number of function evaluation
    % lb=([dat(1,1).*ones(1,length(par0(1,:))); 0*ones(1,length(par0(1,:)));...
    %     Gb(1)*ones(1,length(par0(1,:)));Lb(1)*ones(1,length(par0(1,:)));]);
    % ub=([dat(end,1).*ones(1,length(par0(1,:)));inf* ones(1,length(par0(1,:)));...
    %     Gb(2)*ones(1,length(par0(1,:)));Lb(2)*ones(1,length(par0(1,:)))]);
    %
    %%%%% Part of 'fit2voigt_restricted' function:
    % delta = 0.5;
    % maxfeval=150*numel(par0); % maximum number of function evaluation
    % lb=([dat(1,1).*ones(1,length(par0(1,:))); ... 
    %     0*ones(1,length(par0(1,:)));...
    %     delta*par0(3,:); ...
    %     delta*par0(4,:)]);
    % ub=([dat(end,1).*ones(1,length(par0(1,:)));...
    %     inf* ones(1,length(par0(1,:)));...
    %     (2-delta)*par0(3,:);...
    %     (2-delta)*par0(4,:)]);


    % Plot the fitting results
    figure(i)
    plot(data(:,1),data(:,2),'b',data(:,1),voigt(data(:,1),parmin),'r');
    hold on
    for k = 1:size(par0,2)
        plot(data(:,1), voigt(data(:,1),parmin(:,k)))
        % Save peak intensity
        peakint(:,k) = voigt(parmin(1,k),parmin(:,k));
    end
    
    hold off
    legend('data','fit')
    
    % Combine the peak intensity and the fitted parameter (parmin)
    coef = [parmin; peakint];
    % Set the save file name
    savename = [filenames(i).folder '/coef/coef_' filenames(i).name];
    % Save the fitted parameter and peak intensity
    csvwrite(savename,coef);

    pause;
end
