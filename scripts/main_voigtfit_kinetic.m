% Fit the absorbtion spectra of a mixture of SWCNTs as a sum of spectra of
% purified SWCNT species using voigt profile. The spectra of kinetic were
% fitted based on the initial fitting parameter obtained by SDC/SWCNT+DNA
% at low temperature (5 oC).

% 'voigt line shape fit' package and 'fadf' function are required to run 
% this mfile.
% 'voigt line shape fit' package can be downloaded from the following link:
% https://www.mathworks.com/matlabcentral/fileexchange/57603-voigt-line-shape-fit
% 'fadf' function can be downloaded from the following link:
% https://www.mathworks.com/matlabcentral/fileexchange/47801-the-voigt-complex-error-function-second-version

clear all; close all; clc;

% Go to the directory and list all filenames in the directoryclear all;
cd ../data/SDC_EG150X_40MeOH/blcorrected/
filenames = dir;

cd ../../../scripts/

% wavelength measurement step (usually 0.5 or 1 nm)
wlspan = 1; 
% Set fitting range
fitstart = (1090-1080)/wlspan;     % End wavelength is 1080 nm
fitend = (1090-910)/wlspan;       % Start wavelength is 910 nm

% Set SWCNT species
cnt = {'(9,1)','(8,3)','(6,5)','(7,5)'};


for i = 4:(size(filenames,1)-1)
    % Read csv files. col 1 is wavelength and col 2 is the absorbance. 
    rawdata = csvread([filenames(i).folder '/' filenames(i).name],1,0);
    % This is the range of wavelengths and absorbance we want to fit
    data = rawdata(fitstart:fitend,:);
    
    % Load the initial parameters, par0. The initial fitting parameter 
    % obtained by SDC/SWCNT+DNA at low temperature (5 oC).  
    % 1st row is peak position, 2nd row is intensity height, third row is 
    % Gaussian width, and 4th row is Lorentzian width. 
   
    par0 = csvread([filenames(i).folder(1:end-11) 'control_sample/', ...
        filenames(i).folder(end-10:end) '/coef/coef_', ...
        filenames(i).name(1:end-12) '5C.csv']);

    % Assign the figure to the varable 'fig' to save the figure later
    fig(i) = figure(i);
    
    % Set the variable for initial parameter 
    %       - get rid of the last row (peak intensity)
    par_init = par0(1:4,:);
    
    for j = 2:size(data,2)
        % Fit the spectra using Voigt profile
        [parmin,resnom,res,exitflag]= fit2voigt_restricted([data(:,1),data(:,j)],par_init);
        % Function 'fit2voigt_restricted' is the modifed version of 
        % 'fit2voigt' function. We restricted some of the fitting 
        % parameters, Lorentzian width and Gaussian width, to be in a range 
        % of [-0.5*(original value), 0.5*(original value)]. 
        % Thus, the following scripts was changed in 'fit2voigt' function:
        % 
        %%%%% Part of original 'fit2voigt' function:
        % Lb=[0.001 15];  % Lorentzian width range [lower upper]
        % Gb=[0.1 15];     % Gaussian width range [lower upper]
        % maxfeval=150*numel(par0); % maximum number of function evaluation
        % lb=([dat(1,1).*ones(1,length(par0(1,:))); 0*ones(1,length(par0(1,:)));...
        %     Gb(1)*ones(1,length(par0(1,:)));Lb(1)*ones(1,length(par0(1,:)));]);
        % ub=([dat(end,1).*ones(1,length(par0(1,:)));inf* ones(1,length(par0(1,:)));...
        %     Gb(2)*ones(1,length(par0(1,:)));Lb(2)*ones(1,length(par0(1,:)))]);
        %
        %%%%% Part of 'fit2voigt_restricted' function:
        % delta = 0.5;
        % maxfeval=150*numel(par0); % maximum number of function evaluation
        % lb=([dat(1,1).*ones(1,length(par0(1,:))); ... 
        %     0*ones(1,length(par0(1,:)));...
        %     delta*par0(3,:); ...
        %     delta*par0(4,:)]);
        % ub=([dat(end,1).*ones(1,length(par0(1,:)));...
        %     inf* ones(1,length(par0(1,:)));...
        %     (2-delta)*par0(3,:);...
        %     (2-delta)*par0(4,:)]);
    
        for k = 1:size(par0,2)
            drawnow;
            % Plot the peak intensity change
            coef_int(j-1,k) = voigt(parmin(1,k),parmin(:,k));
            subplot(2,2,k); plot((j-2)*2, parmin(1,k)-par0(1,k),'.k');
            axis([0 120 -5 10])
            title(cnt{k})
            hold on;
        end
        
        % Set the initial fitting parameter for next spectra as the fitting
        % parameter obtained in previous spectra
        par_init = parmin;
        
        % Save the peak position
        coef_wl(j-1,:) = parmin(1,:);
    end
    
    % Set time span (min)
    time = 0:2:120;
    % Calculate peak shift and intensity ratio
    coef_dwl = coef_wl-par0(1,:);
    coef_dint = coef_int./par0(5,:);

    % Set the save file name
    savename = [filenames(i).folder(1:end) '/coef/coef_' filenames(i).name(1:end-4)];
    % Save the peak shift, intensity, and figures
    csvwrite([savename '_wl.csv'],[time' coef_dwl]);
    csvwrite([savename '_int.csv'],[time' coef_dint]);
    savefig(fig(i),savename);
end
