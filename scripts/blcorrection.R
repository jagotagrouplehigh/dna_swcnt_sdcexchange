# Script Name: blcorrection.R
# Purpose: This script reads in csv files and perform baseline correction.
#          The wavelength range should be matched in all files. 
# Authors: Yoona Yang
# License: Creative Commons Attribution-ShareAlike 4.0 International License.
##########
# Latest Changelog Entires:
# v0.00.01 - leg-blcorrection.R - Yoona Yang started this R script

library(ggplot2)
library(ggfortify)
library(hyperSpec)
library(baseline)

# Set path
pathdata <- "./data/SDC_EG150X_40MeOH/"

# Read metadata file
filename <- list.files(pathdata, pattern="meta", full.names=TRUE)
meta.df <- read.csv(filename) 

# Initialize matrix
abs.mat <- NULL

# Set the data range
wl.start <- 300  # start from 300 nm
wl.end <-1090    # to 1090 nm

for (i in 1:length(meta.df$dna)) {
  # Read csv file
  rawdata <- read.csv(as.character(meta.df$path[i]))
  if (length(rawdata[1,]) > 62) {rawdata <- rawdata[,1:62]} # Use only 2 hr (61 spectra) data
  
  abs.mat <- cbind(abs.mat, as.matrix(rawdata[,-1]))  # extract the absorbace (col2)
  
  nstep <- length(abs.mat[1,]) # number of measurements (61 for kinetic data)
  # Create metadata for single sample
  singlemeta.df <- meta.df[rep(i,each=nstep),] 
  singlemeta.df$step <- cbind(singlemeta.df$step,c(1:nstep))
  
  wl <- rawdata[,1]  # wavelength
  Nwl <- length(wl)  # number of wavelength
  redwl <- wl[c((Nwl+200-wl.end):(Nwl+200-wl.start))]  # reduced range of wavelength
  redabs.mat <- abs.mat[c((Nwl+200-wl.end):(Nwl+200-wl.start)),]  # reduced range of absorbance
  # Set hyperspec variable for single sample
  absdata <- new("hyperSpec", spc = t(redabs.mat), wavelength = redwl, data = singlemeta.df) 
  
  # Baseline correction using "baseline" package
  bl2 <- baseline::baseline (absdata$spc, method = "modpolyfit", degree = 2)
  abs.blcor2 <- baseline::getCorrected (bl2)
  abs.blcor2 <- t(as.data.frame(abs.blcor2))
  colnames(abs.blcor2)  <- colnames(rawdata)[2:length(colnames(rawdata))]
  data.blcor <- cbind(redwl,abs.blcor2)
  
  # Set the save name
  tempname <- stringr::str_split(as.character(meta.df$path[i]),'/')[[1]]
  tempname <- c(tempname, "blcorrected")
  tempname <- c(tempname[1:(length(tempname)-2)],tempname[length(tempname)],tempname[(length(tempname)-1)])
  savename <- NULL
  for (k in 1:(length(tempname)-1)) {
    savename <- c(savename, tempname[k], '/')
  }
  savename <- paste0(c(savename, tempname[length(tempname)]),sep="",collapse="")
  
  # Save the baseline corrected absorbance
  write.csv(data.blcor,savename,row.names = F)
  
  # # Plot baseline and baseline corrected spectra
  # plot(redwl,absdata$spc[1,],type ="l",ylim=c(0,1.5))
  # lines(redwl,bl2@baseline[1,],col="blue",lty=2)
  # lines(redwl,abs.blcor2[1,],col="blue")
  # lines(redwl,bl22,col="red")
  
  # Initialize the variables
  data.blcor <- NULL
  abs.mat <- NULL
}


